# MassSpecPlots.jl Documentation

*MassSpecPlots.jl* is a Julia package providing tools for plotting mass spectrometry data.

This package complements [MassSpec.jl](https://massspec.jl.gitlab.io/MassSpec.jl/).

## Installation
```julia-repl
julia> import Pkg; Pkg.add("MassSpecPlots")
```

## Code source

[MassSpecPlots.jl](https://gitlab.com/massspec.jl/massspecplots.jl)

## Reporting issues

[durif@kth.se](mailto:durif@kth.se)
