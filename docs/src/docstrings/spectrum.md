# Spectrum

## Functions

```@docs
MassSpecPlots.spectrumPlot
MassSpecPlots.spectrumPlot!
MassSpecPlots.spectraPlot2D
MassSpecPlots.spectraPlot2D!
MassSpecPlots.spectrumPlotAnimated
MassSpecPlots.spectraPlotAnimated
```

## Structures

```@docs
MassSpecPlots.SpectrumPlotAttributs
```

