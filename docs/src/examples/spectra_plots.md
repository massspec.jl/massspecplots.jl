```@meta
DocTestSetup = quote
    using MassSpec, MassSpecPlots, Plots
    traces = LoadTraceFile("./src/examples/data/traces.h5")
    rawData = LoadRawData("./src/examples/data/data.h5");
end
```

# Plots spectra

## [Simple spectrum](@id spectrum_example)

Spectra are computed using the `computeSpectrum` function from the `MassSpec.jl` package.

### Plot spectrum

```@example spectrum
using MassSpec, MassSpecPlots

spectrum = LoadSpectrumFile("./data/spectra/CH3I_lights_ON.txt");
residual = LoadSpectrumFile("./data/spectra/CH3I_lights_ON_with_NO.txt");

spectrumPlot(spectrum; backend=:GR)
spectrumPlot!(residual)
```

### Plot specific mass

```@example spectrum
spectrumPlot(spectrum; mass="C3H6OH+", backend=:GR)
```

### Background substracted

```@example spectrum
spectrum_background_substracted = computeSpectrum(spectrum; background=residual, integer=true)
spectrumPlot(spectrum_background_substracted; backend=:GR)
```

### Background divided

```@example spectrum
spectrum_background_divided = computeSpectrum(spectrum;  background=residual, relative=true, threshold=100, integer=true)
spectrumPlot(spectrum_background_divided; backend=:GR)
```

### Annotate the spectrum

Display in y-axis log scale and annotate *num_peaks* highest peaks, but not the peaks *m/z* 29, 31; and add annotatation for the peak at 331.

Additionnally, use `Plots` features.

```@example spectrum
using MassSpecPlots, Plots

spectrumPlot(spectrum_background_substracted; annotate=true, num_peaks=12, not_annotated=(29,31), annotated_list=(331), backend=:GR)
plot!(frame=:semi, ylims=(-1.5e4,2.5e4))
```

## [Mutiples spectra with z-axis](@id spectra_example)

```@example spectrum
sample,background,spectrum=Dict(),Dict(),Dict()
for i in 1:4
    sample["$i"]=LoadSpectrumFile("./data/spectra/sample$i.txt")
    background["$i"]=LoadSpectrumFile("./data/spectra/background$i.txt")
    spectrum["$i"]=computeSpectrum(sample["$i"]; background=background["$i"], integer=true, relative=true, scale_to_1=true, threshold=10)
end

velocity=(1,2,3,4)

spectraPlot2D((spectrum["1"],spectrum["2"],spectrum["3"],spectrum["4"]), y=velocity, backend=:GR)
plot!(size=(800,800), xticks=(0:30:300), thickness_scaling=1.3)
xlims!(0,300)
xlabel!("m/z")
zlabel!("signal/background")
ylabel!("velocity")
```

## [Time dependent spectrum](@id animated_spectrum_example)

```@example spectrum
raw=LoadRawData("./data/data.h5");

spectrumRange=78:197
background=computeSpectrum(raw;range=50:74)
title=[i in 108:168 ? "Photolysis ON" : "Photolysis OFF" for i in 78:197]

spectrumPlotAnimated(raw, spectrumRange; background=background, integer=true, scale_to_1=false, relative=true, threshold=30, forceFitMasses=0.5, backend=:gr, average=1, size=(800,400), dpi=100, xticks=(0:20:300), thickness_scaling=1.3, xlims=(0,300), fps=12, ylims=(0,10), xlabel="m/z", ylabel="signal/background", title=title, format=:gif)
```

## [Time dependent spectra](@id animated_spectra_example)

```@example spectrum

spectrum1=78:197
spectrum2=376:495
spectrum3=780:899
spectrum4=978:1097
spectra=(spectrum4,spectrum3,spectrum2,spectrum1)

background1=65:74
background2=365:374
background3=770:779
background4=969:978
background=(computeSpectrum(raw;range=background4),computeSpectrum(raw;range=background3),computeSpectrum(raw;range=background2),computeSpectrum(raw;range=background1))

yaxis=(1,2,3,4)
title=[i in 30:90 ? "Photolysis ON" : "Photolysis OFF" for i in 1:120]

spectraPlotAnimated(raw, spectra; y=yaxis, background=background, integer=true, scale_to_1=false, relative=true, threshold=30, forceFitMasses=0.5,
            backend=:gr, average=1, size=(800,800), dpi=100, xticks=(0:30:300), thickness_scaling=1.3, xlims=(0,300), zlims=(0,10), fps=12, format=:gif,
            xlabel="m/z", ylabel="spectra", zlabel="signal/background", title=title, legend=nothing)
```
