```@meta
DocTestSetup = quote
    using MassSpec, MassSpecPlots, Plots
    raw = LoadRawData("./src/examples/data/data.h5")
end
```

# [Traces plots](@id trace_example)

```@example trace
using MassSpec, MassSpecPlots, Plots

raw = LoadRawData("./data/data.h5")
trace(raw, "m48"; norm="m19", average=5, indications="./data/indications.txt")
```
<!--=> revoir "indications.txt"-->


```@example trace
cycles = json2dict("./data/RangeSetting.json")
trace(raw, "m48"; norm="m19", average=5, backend=:plotly) # cycles=cycles,
```

<!--traces(traces,"m48"; xlims=(1,0),norm=false,time=false,indications=true, backend=:plotly, cycles=(Copper, pts=15, background=(10,2)))

Note: add documentation about `cycles=(Copper, pts=15, background=(10,2))`-->
