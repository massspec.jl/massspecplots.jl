# [Heatmap plot](@id plot_heatmap)

Signal vs mass vs experiment time plot as heatmap with [`trace2D`](@ref).

```@example heatmap
using MassSpec, MassSpecPlots, Plots
rawData = LoadRawData("./data/data.h5");

timeRange=31:1230
background=1:30

trace2D(rawData; mz=(5,200), cycle=timeRange, resize=(400,400), background=background, color=:diverging_rainbow_bgymr_45_85_c67_n256, threshold=1, zscale=:log, backend=:gr)
xlabel!("m/z")
ylabel!("Time (s)")
plot!(colorbar_title="Signal (cps)")

zlims!(0,5)
zticks!([0,100,1000,10000,10000])
xticks!(0:20:200)
yticks!(0:200:1200)
plot!(dpi=150, size=(500,300))
```
