using MassSpecPlots, Documenter

makedocs(
        modules = Module[MassSpecPlots],
        sitename = "MassSpecPlots.jl",
        authors= "Olivier Durif",
        pages = [
            "Home" => "index.md",
            "Examples" => [
                "Spectra Plots" => "examples/spectra_plots.md",
                "Traces Plots" => "examples/traces_plots.md",
                "Heatmap Plots" => "examples/heatmap_plots.md"
                    ],
            "Docstrings" => [
                "spectrum.jl" => "docstrings/spectrum.md"
                "traces.jl" => "docstrings/traces.md"
                    ],
            ],
        #repo = "https://gitlab.com/massspec.jl/massspecplots.jl/blob/{commit}{path}#{line}",
        remotes = nothing,
        format = Documenter.HTML(
            #assets = ["assets/custom.css", "assets/favicon.ico"],
            prettyurls = true,
            canonical = "https://massspec.jl.gitlab.io/MassSpecPlots.jl/",
        ),
)

deploydocs(
     repo = "gitlab.com/massspec.jl/massspecplots.jl.git",
     devbranch = "main",
)
