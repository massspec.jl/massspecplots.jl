"""
    trace(raw::MassSpec.RawData, mass::AbstractString; kwargs...)

Alias for [`trace`](@ref MassSpecPlots.trace) function, using `RawData`.

# Arguments
- `raw::MassSpec.RawData`
- `mass::AbstractString`

# Optionals arguments
See [`MassSpecPlots.TracePlot`](@ref)
```

# Example

See [traces plots](@ref trace_example).
"""
function trace(raw::MassSpec.RawData, mass::AbstractString; kwargs...)

    trace(raw.traces,mass; kwargs...)

end


"""
    trace!(raw::MassSpec.RawData, mass::AbstractString; kwargs...)

Alias for [`trace!`](@ref MassSpecPlots.trace) function, using `MassSpec.RawData`.
"""
function trace!(raw::MassSpec.RawData, mass::AbstractString; kwargs...)

    trace!(raw.traces,mass; kwargs...)

end

"""
     TracePlot <: Plot

Encapsulates parameters used for trace function.

# Members
- `mass::AbstractString`: mass to be traced
- `cycles::Union{NamedTuple,Dict,Nothing,Tuple{Vararg{Dict,N} where N}}=nothing`: Highlight the ranges into a dictionnary of cycles
- `norm::Union{Bool,AbstractString,Vector,Tuple,Symbol,Nothing}=nothing`: Normalization of the signal
- `xlims::Union{Tuple{Int64,Int64},UnitRange{Int64},Nothing}=(1,0)`:
- `range::Union{UnitRange{Int64},Dict{Any,Any},Nothing}=nothing`: Display only the range
- `background::Union{Tuple{Int64,Int64},UnitRange{Int64},Nothing}=nothing`: Background to be substracted. Tuple first argument is the number of pts to be averaged while second argument is the number of pts beforer the cycle UnitRange.
- `time::Bool=false`: Abscissa coordinate
- `label::Union{AbstractString,Symbol}=:auto`:
- `title::AbstractString=""`:
- `average::Int64=1`: Average multiples bins
- `scale::Union{Int64,Float64}=1`: y axis amplitude factor into the signal
- `cut::Union{Tuple{Int64, Int64},Nothing}=nothing`: Cut a part of the signal
- `color::Union{Symbol,Int64}=:auto`:
- `line::Union{Symbol,Int64,Float64}=2`:
- `w::Union{Int64,Float64}=2`:
- `stime::Bool=false`:
- `ylims::Union{Tuple{Float64,Float64},Tuple{Int32,Int32}}`: minimum and maximum value of the trace
- `backend::Symbol=:auto`: Backend supported: gr, plotly, gaston, inspectdr, pgfplotsx
- `indications::Union{Bool,AbstractString,Nothing}=nothing`: Plot lights cycles contains into `indications.txt` file
- `annotate::Bool=true`: Annotate the plot according to informations contains into `indications.txt` file
- `add::Bool=false`: Add another mass, alias for trace!
- `debug::Bool=false`:
"""
@kwdef mutable struct TracePlot <: Plot
    mass::AbstractString
    cycles::Union{NamedTuple,Dict,Nothing,Tuple{Vararg{Dict,N} where N}}=nothing
    norm::Union{Bool,AbstractString,Vector,Tuple,Symbol,Nothing}=nothing
    xlims::Union{Tuple{Int64,Int64},UnitRange{Int64},Nothing}=(1,0) ## TODO: implement =nothing here
    range::Union{UnitRange{Int64},Dict,Nothing}=nothing
    background::Union{Tuple{Int64,Int64},UnitRange{Int64},Nothing}=nothing
    time::Bool=false
    label::Union{AbstractString,Symbol}=:auto
    title::AbstractString=""
    average::Int64=1
    scale::Union{Int64,Float64}=1
    cut::Union{Tuple{Int64, Int64},Nothing}=nothing
    color::Union{Symbol,Int64}=:auto
    line::Union{Symbol,Int64,Float64}=2
    w::Union{Int64,Float64}=2
    stime::Bool=false
    ylims=nothing
    backend::Symbol=:auto
    indications::Union{Bool,AbstractString,Nothing}=nothing
    annotate::Bool=true
    add::Bool=false
    debug::Bool=false
end

@kwdef mutable struct PlotOptions <: Plot
end

#TODO> add relative background
# TODO> xlims=nothing
## Add cut::UnitRange{Int64}
"""
    trace(f::MassSpec.TraceData, mass::AbstractString; kwargs...)

Plot integrated mass signal vs time.

# Arguments
- `f::MassSpec.TraceData`
- `mass::AbstractString`

# Optionals arguments
See [`MassSpecPlots.TracePlot`](@ref)

# Example

See [traces plots](@ref trace_example).
"""
function trace(f::MassSpec.TraceData, mass::AbstractString; kwargs...)

    # arguments as attributs
    attributs=TracePlot(mass=mass)
    for (k,v) in kwargs
         isdefined(attributs,k) ? setproperty!(attributs,k,v) : @warn "Unexpected $k argument"
    end

    # define the backend for the plot
    traceBackend(attributs.backend,indications=attributs.indications,annotate=attributs.annotate)

    # process and plot
    trace!(f,mass; kwargs...)

end

"""
    trace!(f::MassSpec.TraceData, mass::AbstractString; kwargs...)

Add a curve of an integrated mass signal vs time.
"""
function trace!(f::MassSpec.TraceData, mass::AbstractString; kwargs...)

    ## TODO: label if range
    ## TODO: add range tuples inside of a dict !!!
    ## TODO: add xlims in cycles

    # arguments as attributs
    attributs=TracePlot(mass=mass)
    for (k,v) in kwargs
         isdefined(attributs,k) ? setproperty!(attributs,k,v) : @warn "Unexpected $k argument"
    end

    if attributs.indications==true
        attributs.indications=="indications.txt" ## TODO> checker file exist
    end

    # check the incompatibilities
    # TO ADD: no background possible if cycles argument ???
    if attributs.range != nothing && attributs.cycles != nothing
        @error "Choose either 'range' or 'cycles' arguments"
    elseif attributs.range != nothing && (attributs.xlims != nothing && attributs.xlims != (1,0))
        @error "Cannot specified 'xlims' if 'range' is defined"
    end

    # check the mass name is conform -> TODO change m59.000 => m59 + displayed a warning if the mass is changed
    #mass=massName(f.masses,mass)

    # check the range and convert UnitRange into a Dict of UnitRange
    if attributs.range==nothing
        attributs.range=Dict("range" => tuple(1:length(f.time)))
    elseif isa(attributs.range,UnitRange{Int64})
        attributs.range=Dict("range" => tuple(attributs.range))
    end

    data=Float32[]

    # do a loop and plot each curve in range
    for ranges in attributs.range ## into the Dict

        if isa(ranges[2],UnitRange{Int64})
            ranges=tuple(ranges[2]) ## convert the UnitRange{Int64} into the Dict to a tuple of UnitRange{Int64}
        elseif !isa(ranges[2],Tuple)
            @error "range is not a Dict of UnitRange{Int64} nor a Dict of Tuple; type is $(typeof(range[2]))"
        else
            ranges=ranges[2]
        end

        # iterate over the tuple of UnitRange{Int64}
        for range in ranges ## access to the UnitRange{Int64} into the tuple (which is itself the value into the Dict)

            if !isa(range,UnitRange{Int64})
                @error "range inside the Dict of Tuple is not of UnitRange{Int64} type; type is $(typeof(range))"
            end

            # data to be traced (masses)
            data=f.data[f.masses[mass],:][range]

            # substract background
            if isa(attributs.background,Tuple{Int64,Int64}) ## convert the type
                attributs.background=range[1:attributs.background[1]].-(attributs.background[1]+attributs.background[2]) ## tack background as n pts before the range
            end
            if isa(attributs.background,UnitRange{Int64})
                data=data.-mean(f.data[f.masses[mass],attributs.background])
            end

            # normalization
            data=data./normalization(f,attributs.norm;range)

            # compute xlims:: TODO> case xlims=nothing
            xstart = Int(attributs.xlims[1])
            if attributs.xlims[2]==0
                xstop = length(data)
            elseif attributs.xlims[2]<0
                xstop = length(data) + Int(attributs.xlims[2])
            else
                xstop = Int(attributs.xlims[2])
            end
            data = data[xstart:xstop]

            # apply a scale factor
            data=data.*attributs.scale

            # abscisse -> time
            if attributs.time && !attributs.stime
                time=f.time[xstart:xstop]
            elseif attributs.time && attributs.stime
                time=f.time[1:xstop-xstart]
            else
                time=1:length(data)
            end

            # cut some parts of the data ## TODO: cut tuple of UnitRange
            if attributs.cut != nothing
                #isnothing(attributs.time) ? time=nothing : time=[time[1:attributs.cut[1]];time[attributs.cut[2]:end]]
                data=[data[1:attributs.cut[1]];data[attributs.cut[2]:end]]
            end

            # average
            if attributs.average > 1
                data=[mean(data[i:i+attributs.average]) for i in 1:attributs.average:Int64(floor(size(data)[1]/attributs.average)*attributs.average)]
                time=time[1:attributs.average:end-attributs.average+1]
            end

            # choose the right color
            attributs.color == :auto && mass in keys(f.attributes.color) ? attributs.color=f.attributes.color[mass] : nothing

            # label the plot
            if attributs.label == :auto && mass in keys(f.attributes.molecules)
                attributs.label = mass * " ($(f.attributes.molecules[mass]))"
            elseif !isa(attributs.label,AbstractString)
                attributs.label=""
            end

            plot!(time, data, label=attributs.label, color=attributs.color, line=attributs.line, w=attributs.w)

        end

    end

    attributs.title == nothing ? nothing : attributs.title=""

    if attributs.cycles != nothing
        cycles!(attributs.cycles) ## TODO> add xlims, ylims
    end

     if isa(attributs.indications, AbstractString)
         attributs.ylims=(minimum(filter(!isnan,data)), maximum(filter(!isinf,filter(!isnan,data)))) ## TODO> to be moved somewhere else
         indications!(xlims=attributs.xlims, annotate=attributs.annotate, ylims=attributs.ylims, filePath=attributs.indications);
     end

    if attributs.debug
        return data,attributs
    end

    plot!(title=attributs.title)

end
