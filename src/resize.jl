## resize a matrix for plotting (matrix [m/z:cycle])
function resizeMatrix(matrix::Union{Matrix{Float64},Matrix{Float32}}; resize=(200,100))

    # check matrix sizes
    if size(matrix)[1]<resize[1] || size(matrix)[2]<resize[2]
        @error "Can't reduce the matrix because new matrix size is larger. Check your resize parameter."
    end

    # check y_resize is an integer multiplier of y_matrix
    if size(matrix)[2]%resize[2] != 0
        @warn "Matrix resize failed because y_resize (new cycle size) is not an interger multiplier of the cycle range used. Could need to reduce the cycle parameter by n-1."
    end

    # first, define new matrix size
    nx = resize[1]
    ny = resize[2]
    matrix_resized = Array{Float64}(undef,nx,ny)

    dx=size(matrix)[1]/nx
    dy=size(matrix)[2]/ny

    for i ∈ 1:nx, j ∈ 1:ny
        x=floor(Int, (i-1)*dx+1):floor(Int, i*dx)
        y=floor(Int, (j-1)*dy+1):floor(Int, j*dy)

        matrix_resized[i,j] = sum(matrix[x,y])
    end

    return matrix_resized./dy # so that m/z is summed and cycle is averaged

end

## resize a vector for plotting by summing pts
function resizeVector(vector::Union{Vector{Float64},Vector{Float32}}; resize=2000)

    # check vector sizes
    if length(vector)<resize
        @error "Can't reduce the vector because new vector size is larger. Check your resize parameter."
    end

    vector_resized = Array{Float64}(undef,resize)
    dx=length(vector)/resize

    for i ∈ 1:resize
        x=floor(Int, (i-1)*dx+1):floor(Int, i*dx)
        vector_resized[i] = sum(vector[x])
    end

    return vector_resized

end
