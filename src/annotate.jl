## annotate spectrum
function spectrum_annotate(spectrum::DataFrame; num_peaks::Int64=10, police::Int64=10, not_annotated::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing}=nothing, annotated_list::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing}=nothing)

    peaks=listPeaks(spectrum; annotated_list=annotated_list, num_peaks=num_peaks)
    for (mass,height) in peaks
        height<0 ? position=:top : position=:bottom
        if not_annotated == nothing || !(mass in not_annotated)
            annotate!(mass, height, text(string(Int(mass)), color, position, police, "Helvetica Bold"))
        end
    end

end

# Note: super bad render for now
function spectrum2D_annotate(spectrum::DataFrame, y::Real; num_peaks::Int64=10, police::Int64=12, not_annotated=(), annotated_list=(), backend::Symbol=:plotly)

    (backend == :gr) ? (@warn "3D annotation not implemented to date with GR backend. Please use plotly.") : nothing

    peaks=listPeaks(spectrum; annotated_list=annotated_list, num_peaks=num_peaks)
    for (mass,height) in peaks

        height<0 ? position=:top : position=:bottom
        if not_annotated == nothing || !(mass in not_annotated)
            #annotate!(mass, y, height, text(string(Int(mass)), position, police))
            annotate!(mass, y, z=height, string(Int(mass)))
        end
    end

end

"""
    trace_annotate(indications::MassSpecPlots.IndicationsTrace; color::Symbol=:dark, police::Int64=12, rotation::Int64=90, _max::Union{Int64,Float64}=1e3, shift::Union{Int64,Float64}=0)

Annotate trace.
"""
function trace_annotate(indications::MassSpecPlots.IndicationsTrace; color::Symbol=:dark, police::Int64=12, rotation::Int64=90, _max::Real=1e3, shift::Real=0)

    for (cycle,comment) in zip(indications.cycles,indications.comments)
        if lowercase(comment)!="light on" && lowercase(comment)!="lights on" && lowercase(comment)!="light off" && lowercase(comment)!="lights off"
            annotate!(cycle-shift, _max, Plots.text(comment, police, color, rotation=rotation))
        end
    end

end
