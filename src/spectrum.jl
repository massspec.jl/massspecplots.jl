"""
     @kwdef mutable struct SpectrumPlotAttributs <: Plot

Encapsulates parameters used for spectrum function.

# Members
- `mass::Union{String,Real,Nothing} = nothing`
- `font::AbstractString = "Computer Modern"`
- `framestyle::Symbol = :box`
- `yaxis::Symbol = :identity`: :log  (TODO: perform a log-modulus transformation, if any negative value)
- `color::Symbol = :auto`
- `w::Int64 = 1`
- `backend::Symbol = :plotly`
- `annotate::Bool = false`
- `num_peaks::Int64 = 10`
- `not_annotated::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing} = nothing`
- `annotated_list::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing} = nothing`
- `print::Union{Int64,Nothing} = 0`
- `ylims::Union{Symbol,Tuple{Int64, Int64}} = :auto`
- `xlims::Union{Symbol,Tuple{Int64, Int64}} = :auto`
- `debug::Bool = false`
- `xticks::Union{Symbol,UnitRange{Int64},StepRange{Int64, Int64},Tuple{UnitRange{Int64}, Vector{String}}} = :auto`
- `yticks::Union{Symbol,UnitRange{Int64},StepRange{Int64, Int64},Tuple{UnitRange{Int64}, Vector{String}}} = :auto`
- `xlabel::AbstractString = ""`
- `ylabel::AbstractString = ""`
- `label::Union{AbstractString,Nothing} = nothing`
- `title::AbstractString = ""`
- `legend::Union{AbstractString, Nothing, Bool} = nothing`
"""
@kwdef mutable struct SpectrumPlotAttributs <: Plot
    mass::Union{String,Real,Nothing} = nothing
    font::AbstractString = "Computer Modern"
    framestyle::Symbol = :box
    yaxis::Symbol = :identity
    color::Union{Symbol,Int64} = :auto
    w::Int64 = 1
    backend::Symbol = :plotly
    annotate::Bool = false
    num_peaks::Int64 = 10
    not_annotated::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing} = nothing
    annotated_list::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing} = nothing
    print::Union{Int64,Nothing} = 0
    ylims::Union{Symbol,Tuple{Real, Real}} = :auto
    xlims::Union{Symbol,Tuple{Real, Real}} = :auto
    debug::Bool = false
    xticks::Union{Symbol,UnitRange{Int64},StepRange{Int64, Int64},Tuple{UnitRange{Int64}, Vector{String}}} = :auto
    yticks::Union{Symbol,UnitRange{Int64},StepRange{Int64, Int64},Tuple{UnitRange{Int64}, Vector{String}}} = :auto
    xlabel::AbstractString = ""
    ylabel::AbstractString = ""
    label::Union{AbstractString,Nothing} = nothing
    title::AbstractString = ""
    legend::Union{AbstractString, Nothing, Bool} = nothing
end

## TODO> add a warning or an error if spectrum is not "integer" but annote is true? or add annotate feature for non-integer spectrum
"""
    spectrumPlot(spectrum::DataFrame; kwargs...)

Plot computed spectrum. Use `MassSpec.computeSpectrum` to compute spectrum.

# Argument
- `spectrumData::DataFrame`

# Optional Arguments
See `MassSpec.SpectrumPlotAttributs`
"""
function spectrumPlot(spectrumData::DataFrame; kwargs...)

    # define the backend for the plot
    attributs=SpectrumPlotAttributs()
    for (k,v) in kwargs
         isdefined(attributs,k) ? setproperty!(attributs,k,v) : @warn "Unexpected $k argument"
    end
    traceBackend(attributs.backend, font=attributs.font, framestyle=attributs.framestyle)

    # plot
    spectrumPlot!(spectrumData; kwargs...) ## TODO> send structure as optionnal argument

end

"""
    spectrumPlot!(spectrum::DataFrame; kwargs...)

# Argument
- `spectrumData::DataFrame`

# Optional Arguments
See `MassSpec.SpectrumPlotAttributs`

# Examples

See [spectrum plots](@ref spectrum_example).
"""
function spectrumPlot!(spectrumData::DataFrame; kwargs...)

    if names(spectrumData) != ["mass", "signal"]
        @error "Please check your spectrum data."
    end

    ## create a dict with the attributs given in kwargs...
    ## TODO > call attributs once for animatedSpectrum
    attributs=SpectrumPlotAttributs()
    for (k,v) in kwargs
         isdefined(attributs,k) ? setproperty!(attributs,k,v) : @warn "Unexpected $k argument"
    end

    if attributs.yaxis==:log && any(spectrumData.signal.<=0)
        @info "Log-modulus transformation"
        spectrumData.signal=sign.(spectrumData.signal).*log10.(abs.(spectrumData.signal).+1) #log-modulus transformation
        yaxis=:identity
        # TODO affichage yticks à revoir, l'echelle n'est pas interessante
    elseif attributs.yaxis==:log
        yaxis=:log
    else
        yaxis=:identity
    end

    if attributs.annotate
        spectrum_annotate(spectrumData, num_peaks=attributs.num_peaks, not_annotated=attributs.not_annotated, annotated_list=attributs.annotated_list)
    end

    ## TODO> add in spectrumPlot (not spectrumPlot!)
    if attributs.mass != nothing
        mass = typeof(attributs.mass) == String ? masses(attributs.mass)["m/z"] : attributs.mass
        if ismissing(mass)
            @error "Please check your `mass`. Must be an ion."
        end
        if attributs.xlims != :auto
            @warn "`xlims` and `mass` arguments are incompatible. `xlims` ignored"
        end
        attributs.xlims=(mass*0.9993,mass*1.0007)
        peakRange=findall(attributs.xlims[1] .<= spectrumData.mass .<= attributs.xlims[2])
        ymin=minimum(spectrumData.signal[peakRange])
        ymax=maximum(spectrumData.signal[peakRange])
        attributs.ylims=(ymin-(ymax-ymin)*0.1,ymax+(ymax-ymin)*0.1)
        plot!([mass,mass],[attributs.ylims[1],attributs.ylims[2]], color=:black, annotation=(:topleft,attributs.mass,16), label=:none)
    end

    if attributs.print>0
        listMasses(spectrumData, background=attributs.background, integer=attributs.integer, scale_to_1=attributs.scale_to_1, relative=attributs.relative, threshold=attributs.threshold, print=attributs.print)
    end

    if attributs.debug
        return spectrumData,attributs
    end

    if typeof(attributs.label)<:AbstractString && isnothing(attributs.legend)
        attributs.legend=true
    end

    plot!(spectrumData.mass, spectrumData.signal; color=attributs.color, w=attributs.w, ylims=attributs.ylims, xlims=attributs.xlims, label=attributs.label, xticks=attributs.xticks, xlabel=attributs.xlabel, ylabel=attributs.ylabel, title=attributs.title, legend=attributs.legend, yaxis=yaxis)

end

## TODO: if background == integer, pour optimiser, il ne faudrait pas recalculer le background a chaque fois !!!! (revoir commentaire obsolete?)
"""
    spectraPlot2D(spectra::Tuple{Vararg{DataFrame,N} where N}; y=(), w=1, annotate=false, num_peaks=10, not_annotated=nothing, annotated_list=nothing, backend=:plotlyj, xlims=:auto, ylims=:auto, zlims=:auto, xticks=:auto, xlabel::AbstractString="", ylabel::AbstractString="", zlabel::AbstractString="", title::AbstractString="", font="Computer Modern", framestyle=:box, legend=nothing, colors=nothing)

Plot with multiples spectras with z-axis.

# Arguments
- `spectra::Tuple{Vararg{DataFrame,N} where N}`

# Optional arguments

# Example

See [mutiples spectra with z-axis](@ref spectra_example).
"""
function spectraPlot2D(spectra::Tuple{Vararg{DataFrame,N} where N}; y=(), w=1, annotate=false, num_peaks=10, not_annotated=nothing, annotated_list=nothing, backend=:plotlyj, xlims=:auto, ylims=:auto, zlims=:auto, xticks=:auto, xlabel::AbstractString="", ylabel::AbstractString="", zlabel::AbstractString="", title::AbstractString="", font="Computer Modern", framestyle=:box, legend=nothing, colors=nothing)

    # y axis
    if length(y)!=length(spectra)
        y=1:length(spectra)
    end

    # define the backend for the plot
    traceBackend(backend, size=(900,900), font=font, framestyle=framestyle)

    spectraPlot2D!(spectra, y=y, w=w,  annotate=annotate, num_peaks=num_peaks, not_annotated=not_annotated, xlims=xlims, ylims=ylims, zlims=zlims, xticks=xticks, xlabel=xlabel, ylabel=ylabel, zlabel=zlabel, title=title, legend=legend, colors=colors)

end

"""
    spectraPlot2D!(spectra::Tuple{Vararg{DataFrame,N} where N}; y=(), w=1, annotate=false, num_peaks=10, not_annotated=nothing, annotated_list=nothing, xlims=:auto, ylims=:auto, zlims=:auto, xticks=:auto, xlabel::AbstractString="", ylabel::AbstractString="", zlabel::AbstractString="", title::AbstractString="", legend=nothing, colors=nothing)
"""
function spectraPlot2D!(spectra::Tuple{Vararg{DataFrame,N} where N}; y=(), w=1, annotate=false, num_peaks=10, not_annotated=nothing, annotated_list=nothing, xlims=:auto, ylims=:auto, zlims=:auto, xticks=:auto, xlabel::AbstractString="", ylabel::AbstractString="", zlabel::AbstractString="", title::AbstractString="", legend=nothing, colors=nothing)

    for (i,spectrum) in enumerate(spectra)
        yi=y[i]*ones(length(spectrum.signal))

        c = colors==nothing ? i : colors[i]
        plot!(spectrum.mass,yi,spectrum.signal, w=w, xlims=xlims, ylims=ylims, zlims=zlims, xticks=xticks, xlabel=xlabel, ylabel=ylabel, zlabel=zlabel, title=title, legend=legend, color=c)

        if annotate
            spectrumPlot2D_annotate(spectrum, y[i], num_peaks=num_peaks, not_annotated=not_annotated, annotated_list=annotated_list)
        end
    end

end

"""
    spectrumPlotAnimated(raw::MassSpec.RawData, range::Union{UnitRange{Int64},Tuple{Vararg{UnitRange{Int64},N} where N}}; background::Union{Nothing,DataFrame}=nothing, integer=false, resolution=1e3, scale_to_1=false, relative=false, threshold=0, w=1, annotate=false, num_peaks=10, not_annotated=nothing, annotated_list=nothing, forceFitMasses=1e-1, backend=:plotlyj, fps=10, average=1, format=:webm, xlims=:auto, ylims::Union{Symbol,Tuple{Int64, Int64}}=:auto, xlabel="", ylabel="", title::Union{AbstractString,Vector{String},Symbol}="", size=:auto, dpi=:auto, xticks=:auto, thickness_scaling=:auto)

Animated spectrum plot (no z axis; m/z vs signal)

# Arguments

# Optional arguments

# Example
See [time dependent spectrum](@ref animated_spectrum_example).
"""
function spectrumPlotAnimated(raw::MassSpec.RawData, range::Union{UnitRange{Int64},Tuple{Vararg{UnitRange{Int64},N} where N}}; background::Union{Nothing,DataFrame}=nothing, integer=false, resolution=1e3, scale_to_1=false, relative=false, threshold=0, w=1, annotate=false, num_peaks=10, not_annotated=nothing, annotated_list=nothing, forceFitMasses=1e-1, backend=:plotlyj, fps=10, average=1, format=:webm, xlims=:auto, ylims::Union{Symbol,Tuple{Int64, Int64}}=:auto, xlabel="", ylabel="", title::Union{AbstractString,Vector{String},Symbol}="", size=:auto, dpi=:auto, xticks=:auto, thickness_scaling=:auto)

    ## TODO display and error if average is not multiple of range

    traceBackend(backend; size=size, dpi=dpi, thickness_scaling=thickness_scaling)#; font=font, framestyle=framestyle)

    ProgressMeter.ijulia_behavior(:clear)
    n = length(range)-1
    p = Progress(n÷average; showspeed=true)

    anim::Animation = Plots.Animation()
    for i in 1:average:n

        ## TODO: check if one "range" is out of range -> take the last value
        spectrumData=computeSpectrum(raw; range=range[i]:range[i-1+average], background=background, integer=integer, resolution=resolution, scale_to_1=scale_to_1, relative=relative, threshold=threshold,forceFitMasses=forceFitMasses)

        if title==:auto
            _title="time: $(Int(round((i+1)/60))) minutes"
        elseif isa(title,Vector{String})
            _title=title[1+Int(round(i/average))]
        end

        ## TODO> tout ce qui concerne le plot et ne change pas, passer dans "traceBackend()" pour ne pas le trouver dans la boucle
        spectrumPlot!(spectrumData, xlims=xlims, ylims=ylims, w=w, annotate=annotate, num_peaks=num_peaks, not_annotated=not_annotated, annotated_list=annotated_list, backend=backend, xticks=xticks, xlabel=xlabel, ylabel=ylabel, title=_title, legend=nothing)
        Plots.frame(anim)

        ProgressMeter.next!(p; showvalues = [(:i,i+average)])
        GC.gc()
        plot()

    end

    if format==:webm
        webm(anim, fps=fps)
    elseif format==:gif
        gif(anim, fps=fps)
    elseif format==:mp4
        mp4(anim, fps=fps)
    end

end

"""
    spectraPlotAnimated(raw::MassSpec.RawData, ranges::Tuple{Vararg{UnitRange{Int64},N} where N}; y=(), background::Union{Nothing,Tuple{Vararg{DataFrame,N} where N}}=nothing, integer=false, resolution=1e3, scale_to_1=false, relative=false, threshold=0, w=1, annotate=false, num_peaks=10, not_annotated=nothing, annotated_list=nothing, forceFitMasses=1e-1, backend=:plotlyj, average=1, fps=10, format=:webm, xlims=:auto, ylims::Union{Symbol,Tuple{Int64, Int64}}=:auto, zlims=:auto, xlabel="", ylabel="", zlabel="", title::Union{AbstractString,Vector{String},Symbol}="", size=:auto, dpi=:auto, xticks=:auto, thickness_scaling=:auto, legend=nothing)

Animated spectra plot with a z-axis.

# Arguments

# Optional arguments

# Example

See [time dependent spectra](@ref animated_spectra_example).
"""
function spectraPlotAnimated(raw::MassSpec.RawData, ranges::Tuple{Vararg{UnitRange{Int64},N} where N}; y=(), background::Union{Nothing,Tuple{Vararg{DataFrame,N} where N}}=nothing, integer=false, resolution=1e3, scale_to_1=false, relative=false, threshold=0, w=1, annotate=false, num_peaks=10, not_annotated=nothing, annotated_list=nothing, forceFitMasses=1e-1, backend=:plotlyj, average=1, fps=10, format=:webm, xlims=:auto, ylims::Union{Symbol,Tuple{Int64, Int64}}=:auto, zlims=:auto, xlabel="", ylabel="", zlabel="", title::Union{AbstractString,Vector{String},Symbol}="", size=:auto, dpi=:auto, xticks=:auto, thickness_scaling=:auto, legend=nothing)

    traceBackend(backend; size=size, dpi=dpi, thickness_scaling=thickness_scaling)

    ## TODO display and error if average is not multiple of ranges

    ProgressMeter.ijulia_behavior(:clear)
    n = maximum(length.(ranges))-1
    p = Progress(n÷average; showspeed=true)

    anim::Animation = Plots.Animation()
    for i in 1:average:n

        spectra=[]
        for (j,range) in enumerate(ranges)

            ## TODO: check if one "range" is out of range -> take the last value
            spectrum=computeSpectrum(raw,range=range[i]:range[i-1+average], background=background[j], integer=integer, resolution=resolution, scale_to_1=scale_to_1, relative=relative, threshold=threshold, forceFitMasses=forceFitMasses)
            spectra=push!(spectra,spectrum)

        end

        if title==:auto
            _title="time: $(Int(round((i+1)/60))) minutes"
        elseif isa(title,Vector{String})
            _title=title[1+Int(round(i/average))]
        end

        spectraPlot2D!(Tuple(spectra), y=y, xlims=xlims, ylims=ylims, zlims=zlims, w=w, annotate=annotate, num_peaks=num_peaks, not_annotated=not_annotated, annotated_list=annotated_list, xticks=xticks, xlabel=xlabel, ylabel=ylabel, zlabel=zlabel, title=_title, legend=legend)
        Plots.frame(anim)

        ProgressMeter.next!(p; showvalues = [(:i,i+average)])
        GC.gc()
        plot()

    end

    if format==:webm
        webm(anim, fps=fps)
    elseif format==:gif
        gif(anim, fps=fps)
    elseif format==:mp4
        mp4(anim, fps=fps)
    end


end
