"""
    traceBackend(backend::Symbol; indications::Union{Bool,Nothing,AbstractString}=nothing, annotate::Bool=true, size::Tuple{Int64,Int64}=(800,350), dpi::Real=100, thickness_scaling::Real=1, font::String="Computer Modern", framestyle::Symbol=:box, w::Int64=2)

Choose the backend for the plot.

# Backend supported
- `gr`
- `plotly`
- `gaston`
- `inspectdr`
- `pgfplotsx`

# Argument
- `backend::Symbol`

# Optional arguments
- `indications::Union{Nothing,AbstractString}=nothing`
- `annotate::Bool`
- `size::Tuple{Int64,Int64}`
- `dpi::Real`
- `thickness_scaling`
- `font::String`
- `framestyle::Symbol`
- `w::Int64`

"""
function traceBackend(backend::Symbol; indications::Union{Bool,Nothing,AbstractString}=nothing, annotate::Bool=true, size::Tuple{Int64,Int64}=(800,350), dpi::Real=100, thickness_scaling::Real=1, font::String="Computer Modern", framestyle::Symbol=:box, w::Int64=2)

    if backend in (:gr,:GR)
        gr();
    elseif backend in (:plotly,:Plotly)
        plotly();
    elseif backend in (:inspectdr,:InspectDR)
        inspectdr();
    elseif backend in (:gaston,:Gaston)
        gaston();
    elseif backend in (:pgfplotsx,:PGFPlotsX)
        pgfplotsx();
    elseif backend==:auto && annotate ## gr backend here because plotly issue with annotate
        gr();
    elseif backend==:auto
        plotly();
    else # par defaut
        @warn "Unknown backend. Plotly forced."
        plotly();
    end
    default(fontfamily=font, framestyle=framestyle, size=size, w=w, dpi=dpi, thickness_scaling=thickness_scaling)
    plot()

end


#--------------------------------------------------------#
# ADD Cycles to a Kinetic Trace
#--------------------------------------------------------#
"""
    cycles!(cycles::Union{NamedTuple,Dict,Tuple{Vararg{Dict,N} where N}})

Highlights cycles on a trace.
"""
function cycles!(cycles::Union{NamedTuple,Dict,Tuple{Vararg{Dict,N} where N}})

    ## TODO> use '@with_kw struct' instead of a 'NamedTuple' (see instead '@with_kw struct Flows' for instance)
    if isa(cycles,NamedTuple) ## NamedTuple should be on the form NamedTuple{(:Dict, :pts, :background), Tuple{Dict{Any, Any}, Int64, Tuple{Int64, Int64}}}

        ## check the form of the NamedTuple (check first argument is a Dict and then than :pts and/or :background exist)
        if isa(cycles[1],Dict)
            dict=cycles[1]
        else
            @error "First argument of the `cycles` NamedTuple must be a Dict"
        end
        if :pts in keys(cycles) && isa(cycles[:pts],Int64)
            pts=cycles[:pts]
        elseif :pts in keys(cycles)
            @warn "In the `cycles` NamedTuple, `pts` must be of Int64 type"
            pts=nothing
        else
            pts=nothing
        end
        if :background in keys(cycles) && isa(cycles[:background],Tuple{Int64, Int64})
            background=cycles[:background]
        elseif :background in keys(cycles)
            @warn "In the `cycles` NamedTuple, `background` must be a Tuple{Int64, Int64}"
            background=nothing
        else
            background=nothing
        end

        displayCycles(dict,pts=pts,background=background)

    elseif isa(cycles,Dict)
        displayCycles!(cycles)

    elseif isa(cycles,Tuple{Vararg{Dict,N} where N}) ## todo change color while iterate ??
        for cycle in cycles
            displayCycles!(cycle)
        end
    end

end

"Display cycles on a trace"
function displayCycles!(dict::Dict; pts::Union{Int64,Nothing}=nothing, background::Union{Tuple{Int64,Int64},Nothing}=nothing)

    for (key,value) in zip(keys(dict),values(dict))

        if isa(value,UnitRange{Int64})
            if isa(pts,Int64)
                range=value[end-pts]:value[end]
            else
                range=value[1]:value[end]
            end
            vspan!([range[1],range[end]], color=:gold2, alpha = 0.6, labels = "Cycles", legend=:none)
            if isa(background,Tuple{Int64,Int64})
                vspan!([value[1]-background[1]-background[2],value[1]-background[2]], color=:grey68, alpha = 0.6, labels = "Background", legend=:none)
            end
        elseif isa(value,Tuple{Vararg{UnitRange{Int64},N} where N})
            for range in value
                if isa(pts,Int64)
                    pts_range=range[end-pts]:range[end]
                else
                    pts_range=range[1]:range[end]
                end
                vspan!([pts_range[1],pts_range[end]], color=:gold2, alpha = 0.6, labels = "Cycles", legend=:none)
                if isa(background,Tuple{Int64,Int64})
                    vspan!([range[1]-background[1]-background[2],range[1]-background[2]], color=:grey68, alpha = 0.6, labels = "Background", legend=:none)
                end
            end
        else
            @warn "Value : $value for $key is neither a UnitRange or a Tuple of UnitRange. Please check your dictionnary."
        end
    end
end
