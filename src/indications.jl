## TO COMPLETE
struct IndicationsTrace <: Plot
    cycles::Vector{Any} #Tuple{Vararg{UnitRange{Int64}}}
    comments::Vector{Any} #Tuple{Vararg{AbstractString}}
    colors::Vector{Any} #Tuple{Vararg{Union{Int64,Symbol}}}
end

"""
    read_indications(;filePath::AbstractString="./indications.txt")

Read the indications files according to a specific pattern. Refer about this pattern in the manual. TBD.
"""
function read_indications(;filePath::AbstractString="./indications.txt")

    cycles,colors,comments=[],[],[];

    if !isfile(filePath)
        return IndicationsTrace(cycles,colors,comments)
    end

    open(filePath) do f

        while ! eof(f)
            s = readline(f)

            try
                cycle,key=split(s,";")
                cycles=push!(cycles,parse(Int,cycle));
                try
                    comment,color=split(key,"##");
                    colors=push!(colors,Symbol(strip(color)));
                    comments=push!(comments,strip(comment))
                catch
                    comments=push!(comments,strip(key))
                    if occursin("no on",lowercase(key)) || occursin("no off",lowercase(key))
                        colors=push!(colors,:green);
                    elseif occursin("lights on",lowercase(key)) || occursin("lights off",lowercase(key))
                        colors=push!(colors,:yellow4);
                    else
                        colors=push!(colors,:black);
                    end
                end
            catch
            end

        end
    end

    return IndicationsTrace(cycles,comments,colors)

end


# TODO> Automatically adapt to the scale of the plot.
"""
    indications!(;filePath::AbstractString="indications.txt", xlims::Union{Tuple{Real,Real},UnitRange{Int64}}=(1,0), ylims::Union{Tuple{Real,Real},UnitRange{Int64}}=(-10, 1e5), w::Real=2, print::Bool=false, annotate::Bool=true, annotate_color::Symbol=:black, annotate_police::Real=10, annotate_rotation::Real=90)

Display experiments informations to `trace`.

Alias for `trace(..., indications=true)`

# Optionals arguments:
- `filePath::AbstractString`:
- `xlims::Union{Tuple{Real,Real},UnitRange{Int64}}`:
- `ylims::Union{Tuple{Real,Real},UnitRange{Int64}}`:
- `w::Real`:
- `print::Bool`:
- `annotate::Bool`:
- `annotate_color::Symbol`:
- `annotate_police::Real`:
- `annotate_rotation::Real`:
"""
function indications!(;filePath::AbstractString="indications.txt", xlims::Union{Tuple{Real,Real},UnitRange{Int64}}=(1,0), ylims::Union{Tuple{Real,Real},UnitRange{Int64}}=(-10, 1e5), w::Real=2, print::Bool=false, annotate::Bool=true, annotate_color::Symbol=:black, annotate_police::Real=10, annotate_rotation::Real=90)

    indications=MassSpecPlots.read_indications(filePath=filePath)

    # TODO: simplify...
    lightON=0
    for (cycle,color,comment) in zip(indications.cycles,indications.colors,indications.comments)
        x=cycle-xlims[1]+1
        if x > 0 ## TODO: add if x < data_plot_size - xlims[2]

            if color==:yellow4 ## vspan in yellow for light indications

                ## attention, il semblerait qu'il y ait un cas particulier ou on allume la lumiere, on et on arrete l'acquisition avant d'eteindre la lumiere alors le vspan ne va ne pas s'afficher...
                if (occursin("light on", lowercase(comment)) || occursin("lights on", lowercase(comment))) && lightON==0
                    lightON=cycle
                elseif (occursin("light off", lowercase(comment)) || occursin("lights off", lowercase(comment))) && lightON != 0
                    vspan!([lightON-xlims[1]+1,cycle-xlims[1]+1], color=:yellow4, alpha = 0.2, labels = "Lights ON", legend=:none)
                    lightON=0
                elseif (occursin("light off", lowercase(comment)) || occursin("lights off", lowercase(comment))) && lightON==0
                    @warn "Lights OFF is found two times consecutively in the indications, see cycle $cycle"
                elseif (occursin("light on", lowercase(comment)) || occursin("lights on", lowercase(comment))) && lightON!=0
                    @warn "Lights OFF is found two times consecutively in the indications, see cycle $cycle"
                end

            else # simple vertical straight line
                plot!([x,x],[ylims[1],ylims[2]],color=color,label=nothing,w=w)
            end

        end
    end

    if annotate
        ## TODO: enlever _max ici et revoir la position des annonations
        ## TODO: revoir le "shift"
        shift=100-xlims[1];
        trace_annotate(indications, color=annotate_color, police=annotate_police, rotation=annotate_rotation, _max=ylims[2]*0.8, shift=shift)
    end

    if print
        comments(indications)
    end

    plot!()

end
