function normalization(f::MassSpec.TraceData, norm::Union{Bool,String,Vector,Tuple,Symbol,Nothing}; range::Union{UnitRange{Int64},Nothing}=nothing)

    if norm==true ## norm by f.correction
        if range != nothing
            norm=f.correction[range]
        else
            norm=f.correction
        end
    elseif isa(norm,String)
        if norm=="false"
            norm=1
        elseif norm=="all"
            norm=f.all_signal_each_cycle
        elseif norm in keys(f.masses)
            norm=f.data[f.masses[norm],:]
        end
        elseif isa(norm,Union{Vector{Real},Tuple{Real}})
        norm=norm
    else
        norm=1
    end

    return norm

end
