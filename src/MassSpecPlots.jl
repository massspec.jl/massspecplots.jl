module MassSpecPlots

using DataFrames, MassSpec, Plots, ProgressMeter, Statistics

export
    indications!,
    trace,
    trace!,
    trace2D,
    spectraPlotAnimated,
    spectraPlot2D,
    spectraPlot2D!,
    spectrumPlotAnimated,
    spectrumPlot,
    spectrumPlot!,
    substractSpectrum

abstract type Plot end

include("./indications.jl")
include("./backend.jl")
include("./annotate.jl");
include("./heatmapPlot.jl")
include("./trace.jl");
include("./trace_tools.jl");
include("./spectrum.jl");
include("./resize.jl")

end
