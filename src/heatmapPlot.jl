#------ "heatmaps" --------#

"""
    trace2D!(f::MassSpec.TraceData, mass::String; range=(), interpolate=1, zscale=nothing)

TBD
"""
function trace2D!(f::MassSpec.TraceData, mass::String; range=(), interpolate=1, zscale=nothing)

    ## check the mass name is conform
    mass=massName(f.masses, mass)

    ## data shape in matrix according to range
    data=zeros(length(range[1]),length(range))
    for i in 1:length(range)
        data[:,i]=f.data[f.masses[mass],range[i]]
    end

    # log scale
    if zscale==:log10 || zscale==:log
        data=log10.(data)
    end

    ## interpolation
    data=Interp2D(data,interpolate)

    plot(data, st=:heatmap)

end

"""
    trace2D(f::MassSpec.TraceData, mass::String; range=(), interpolate=1, zscale=nothing, backend::Symbol=:plotly)

Plot trace heatmap, for a given trace file and a given mass -> see tribology

# Arguments
- `f::MassSpec.TraceData`
- `mass::String`

# Optional arguments
TBD
"""
function trace2D(f::MassSpec.TraceData, mass::String; range=(), interpolate=1, zscale=nothing, backend::Symbol=:plotly)#, norm=nothing, xlims=(1,0), time=true, label=:auto, title="", average=1, factor=1, color=:auto, w=2, stime=false) ## aspect_ratio, color=cgrad([:blue, :white,:red, :yellow]

    traceBackend(backend)

    trace2D!(f, mass; range=range, interpolate=interpolate, zscale=zscale)#, norm=norm, xlims=xlims, time=time, label=label, title=title, average=average, factor=factor, cut=cut, w=w, stime=stime, color=color);

end

## TODO: revoir l'echelle en Z (faire en sorte que le cps soit invariant suivant la taille de la matrice redimentionnee)
## TODO: allow normalization and background substraction !!
# matrix is raw.dat, m/z in amu,cycle in pts
"""
    trace2D(raw::MassSpec.RawData; mz::Union{Tuple{Real,Real},Nothing}=nothing, cycle::Union{UnitRange{Int64},Nothing}=nothing, resize=(200,100), background::Union{Nothing,Vector{Real},UnitRange{Int64}}=nothing, zscale::Symbol=:integer, zlims=:auto, threshold::Real=1, backend::Symbol=:plotly, reverse::Bool=false, color::Symbol=:thermal)

# Arguments
- `raw::MassSpec.RawData`

# Optional arguments
- `mz::Union{Tuple{Real,Real},Nothing}=nothing`
- `cycle::Union{UnitRange{Int64},Nothing}=nothing`
- `resize=(200,100)`
- `background::Union{Nothing,Vector{Real},UnitRange{Int64}}=nothing`
- `zscale::Symbol=:integer`
- `zlims=:auto`
- `threshold::Real=1`
- `backend::Symbol=:plotly`
- `reverse::Bool=false`
- `color::Symbol=:thermal`
- `xaxis::Union{Nothing,Vector{Real}}`=nothing

# Example
See [`plot heatmap`](@ref plot_heatmap)

"""
function trace2D(raw::MassSpec.RawData; mz::Union{Tuple{Real,Real},Nothing}=nothing, cycle::Union{UnitRange{Int64},Nothing}=nothing, resize=(200,100), background::Union{Nothing,Vector{Real},UnitRange{Int64}}=nothing, zscale::Symbol=:integer, zlims=:auto, threshold::Real=1, backend::Symbol=:plotly, reverse::Bool=false, color::Symbol=:thermal, xaxis::Union{Nothing,Vector{Float64}}=nothing)

    traceBackend(backend)

    # convert m/z (amu) in pts
    if mz==nothing
        mz=(1,300) ## TODO> bypass
        mz_pts=1:raw.cycleLength
    else
        a=mean(raw.calibrationParameters[1,cycle])
        b=mean(raw.calibrationParameters[2,cycle])
        mz_inf_pts=b+a*mz[1]^0.5
        mz_sup_pts=b+a*mz[2]^0.5
        mz_pts=Int64(round(mz_inf_pts)):Int64(round(mz_sup_pts))
    end

    if cycle==nothing
        cycle=1:raw.numberCycles
    end

    # resize the matrix before plotting
    data=resizeMatrix(raw.data[mz_pts,cycle]; resize=resize)

    # substract background, background is defined by a cycle range into raw.data
    if isa(background,UnitRange{Int64})
        data.-=resizeVector(vec(mean(raw.data[mz_pts,background], dims=2)), resize=resize[1]) # substract mean background. Average cycle, background matrix -> vector; then resize the vector by summing m/z pts to match data
    end

    # apply threshold
    replace!(x -> x <= threshold ? threshold : x, data)

    # log scale
    if zscale ∈ (:log10,:log)
        data=log10.(data)
    end

    # x,y scale (x is m/z, y is cycle)
    x=mz[1]^0.5:(mz[2]^0.5-mz[1]^0.5)/(size(data)[1]-1):mz[2]^0.5
    x=x.^2 # m/z is quadratic

    if xaxis == nothing
        y=1:(length(cycle)-1)/(size(data)[2]-1):length(cycle) # cycle is linear
    else
        y=xaxis[Int.(round.(1:(length(cycle)-1)/(size(data)[2]-1):length(cycle)))]
    end

    if !reverse
        plot(x,y,transpose(data), st=:heatmap, color=color, clims=zlims)
    else
        plot(y,x,data, st=:heatmap, color=color, clims=zlims)
    end

end
