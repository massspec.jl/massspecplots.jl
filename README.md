# MassSpecPlots

[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://massspec.jl.gitlab.io/massspecplots.jl/)

*MassSpecPlots.jl* is package providing tools for plotting mass spectrometry data.

This package is complementary to [*MassSpec.jl*](https://gitlab.com/massspec.jl/MassSpec.jl).

## Installation
```julia-repl
(@1.10) pkg> add MassSpecPlots
```

## Reporting issues

[durif@kth.se](mailto:durif@kth.se)
